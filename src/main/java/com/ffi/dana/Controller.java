/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.dana;

import com.ffi.dana.model.Body;
import com.ffi.dana.model.BodyCancel;
import com.ffi.dana.model.Head;
import com.ffi.dana.model.Order;
import com.ffi.dana.model.ParameterCancel;
import com.ffi.dana.model.ParameterMapper;
import com.ffi.dana.model.ResponseDana;
import com.ffi.http.HttpUtility;
import com.ffi.param.ParamPayment;
import com.ffi.res.Response;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author KFC SOLUTION
 */
@RestController
public class Controller extends HttpUtility {
    
    @Autowired
    private Environment env;

    @Value("${server.api}")
    private String propUrl;
    
    private final String URL_DATA_LOG = "http://192.168.10.4:8111/datalog/post-log";

    @RequestMapping(value = "/cancel-off", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ApiOperation(value = "Digunakan untuk melakukan pembatalan pembayaran", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public Response cancel(@RequestBody String param) {
        Map<String, Object> dataLog = new HashMap<>();
        Gson gson = new Gson();
        ParamPayment prm = gson.fromJson(param, new TypeToken<ParamPayment>() {
        }.getType());
        String billNo = prm.getOutlet().getOutletCode().concat(prm.getOutlet().getPosCode().concat(prm.getBillNo()));
        prm.setBillNo(billNo);
        dataLog.put("reffNo", billNo);
        ParameterCancel pc = new ParameterCancel(env);
        BodyCancel bc = new BodyCancel(env);
        SignatureUtility su = new SignatureUtility(env);
        bc.setAcquirementId(prm.getApprovalCode());
        bc.setCancelReason("Batal");
        bc.setMerchantTransId(prm.getBillNo());
        pc.setBody(bc);
        Response res = new Response();

        String url = propUrl.concat("/order/cancel.htm");
        System.out.println("URL : "+url);
        Map<String, Object> prmData = new HashMap<>();
        prmData.put("request", pc);
        String prmPost = gson.toJson(prmData);

        String rs = su.agreementCreateSignature(prmPost);
        res.setStatus(Boolean.FALSE);
        String[] rp = env.getProperty("99999999").split("#");
        res.setMessages(rp[1]);
        res.setApprovalCode(rp[0]);
        try {
            dataLog.put("parameter", rs);                        
            String result = postResponse(url, rs);
            dataLog.put("response", result);
            Boolean valid = su.agreementVerifySignature(result);
            if (valid) {
                Map<String, Object> map = gson.fromJson(result, new com.google.gson.reflect.TypeToken<Map<String, Object>>() {
                }.getType());
                Map<String, Object> response = (Map<String, Object>) map.get("response");
                Map<String, Object> bd = (Map<String, Object>) response.get("body");
                Map<String, Object> resultInfo = (Map<String, Object>) bd.get("resultInfo");
                /*
                 * resultStatus
                 * resultCodeId
                 * resultMsg
                 */
                System.out.println("RESPONSE : " + result);
//                ResponseDana rd = new ResponseDana(resultInfo.get("resultCodeId").toString());
                String[] resp = env.getProperty(resultInfo.get("resultCodeId").toString()).split("#");
                res.setApprovalCode(resp[0]);
                res.setMessages(resp[1]);
                if (resultInfo.get("resultStatus").equals("S")) {
                    res.setStatus(Boolean.TRUE);
                    res.setMessages(resp[1]);
                    res.setApprovalCode(bd.get("acquirementId").toString());
                }
            }

        } catch (Exception ex) {
            dataLog.put("response", ex.getMessage());
            ex.printStackTrace();
        }
        new Thread(new Runnable() {

            @Override
            public void run() {
                postResponse(URL_DATA_LOG, gson.toJson(dataLog));
            }
        }).start();
        return res;
    }

    @RequestMapping(value = "/payment/{qrScanCode}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ApiOperation(value = "Digunakan untuk melakukan pembayaran", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public Response payment(@PathVariable String qrScanCode, @RequestBody String param) {
        Map<String, Object> dataLog = new HashMap<>();
        Gson gson = new Gson();
        ParamPayment prm = gson.fromJson(param, new TypeToken<ParamPayment>() {
        }.getType());
        ParameterMapper sh = new ParameterMapper(env);
        Body body = new Body(env);
        String billNo = prm.getOutlet().getOutletCode().concat(prm.getOutlet().getPosCode().concat(prm.getBillNo()));
        prm.setBillNo(billNo);
        dataLog.put("reffNo", billNo);
        body.setSettleBizDate(prm.getOutlet().getTransDate());
        body.getScannerInfo().setDeviceId(prm.getOutlet().getPosCode());
        String outletCode = prm.getOutlet().getOutletCode();
        prm.getOutlet().setOutletCode(outletCode);
        body.getShopInfo().setExternalShopId(outletCode);
        SignatureUtility su = new SignatureUtility(env);
        body.setAuthCode(qrScanCode);
        Order ord = new Order(prm);
        body.setOrder(ord);
        sh.setBody(body);
        Response res = new Response();
        String url = propUrl.concat("/retail/pay.htm");
        System.out.println("URL : "+url);
        Map<String, Object> prmData = new HashMap<>();
        prmData.put("request", sh);
        String prmPost = gson.toJson(prmData);

        String rs = su.agreementCreateSignature(prmPost);
        res.setStatus(Boolean.FALSE);
        String[] rp = env.getProperty("99999999").split("#");
        res.setMessages(rp[1]);
        res.setApprovalCode(rp[0]);
        try {
            dataLog.put("parameter", rs);
            String result = postResponse(url, rs);
            dataLog.put("response", result);
            Boolean valid = su.agreementVerifySignature(result);

            if (valid) {
                Map<String, Object> map = gson.fromJson(result, new com.google.gson.reflect.TypeToken<Map<String, Object>>() {
                }.getType());
                Map<String, Object> response = (Map<String, Object>) map.get("response");
                Map<String, Object> bd = (Map<String, Object>) response.get("body");
                Map<String, Object> resultInfo = (Map<String, Object>) bd.get("resultInfo");
                /*
                 * resultStatus
                 * resultCodeId
                 * resultMsg
                 */
                System.out.println("RESPONSE : " + result);
//                ResponseDana rd = new ResponseDana(resultInfo.get("resultCode").toString());
                String[] resp = env.getProperty(resultInfo.get("resultCodeId").toString()).split("#");
                res.setApprovalCode(resp[0]);
                res.setMessages(resp[1]);
                if (resultInfo.get("resultStatus").equals("S")) {
                    res.setStatus(Boolean.TRUE);
                    res.setMessages(resp[1]);
                    res.setApprovalCode(bd.get("acquirementId").toString());
                }
            }

        } catch (Exception ex) {
            dataLog.put("response", ex.getMessage());
            ex.printStackTrace();
        }
        new Thread(new Runnable() {

            @Override
            public void run() {
                postResponse(URL_DATA_LOG, gson.toJson(dataLog));
            }
        }).start();
        return res;
    }

    @RequestMapping(value = "/query", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ApiOperation(value = "Digunakan untuk melakukan pengecekan pembayaran", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public Response query(@RequestBody String param) throws SignatureException {
        Map<String, Object> dataLog = new HashMap<>();
        Gson gson = new Gson();
        ParamPayment prm = gson.fromJson(param, new TypeToken<ParamPayment>() {
        }.getType());
        String billNo = prm.getOutlet().getOutletCode().concat(prm.getOutlet().getPosCode().concat(prm.getBillNo()));
        prm.setBillNo(billNo);
        dataLog.put("reffNo", billNo);
        Map<String, Object> pc = new HashMap<String, Object>();
        Head hd = new Head(env);
        hd.setFunction(hd.getFunction().concat(".order.query"));
        BodyCancel bc = new BodyCancel(env);

        SignatureUtility su = new SignatureUtility(env);
        bc.setAcquirementId(prm.getApprovalCode());
        bc.setMerchantTransId(prm.getBillNo());
        pc.put("head", hd);
        pc.put("body", bc);
//        String content = gson.toJson(pc);
//        String signature = "signature string";//SignatureGenerator.SHA256withRSA(content, publicKey);
//        pc.put("signature", signature);
        Response res = new Response();
        String url = propUrl.concat("/order/query.htm");
        System.out.println("URL : "+url);
        Map<String, Object> prmData = new HashMap<>();
        prmData.put("request", pc);
        String prmPost = gson.toJson(prmData);

        String rs = su.agreementCreateSignature(prmPost);
        res.setStatus(Boolean.FALSE);
        String[] rp = env.getProperty("99999999").split("#");
        res.setMessages(rp[1]);
        res.setApprovalCode(rp[0]);
        try {
            dataLog.put("parameter", rs);
            String result = postResponse(url, rs);
            dataLog.put("response", result);
            Boolean valid = su.agreementVerifySignature(result);

            if (valid) {
                Map<String, Object> map = gson.fromJson(result, new com.google.gson.reflect.TypeToken<Map<String, Object>>() {
                }.getType());
                Map<String, Object> response = (Map<String, Object>) map.get("response");
                Map<String, Object> bd = (Map<String, Object>) response.get("body");
                Map<String, Object> resultInfo = (Map<String, Object>) bd.get("resultInfo");
                Map<String, Object> statusDetail = (Map<String, Object>) bd.get("statusDetail");
                /*
                 * resultStatus
                 * resultCodeId
                 * resultMsg
                 */
                System.out.println("RESPONSE : " + result);
//                ResponseDana rd = new ResponseDana(statusDetail.get("acquirementStatus").toString());
//                res.setApprovalCode(rd.getResponse());
//                res.setMessages(rd.getMessage());
                String[] resp = env.getProperty(resultInfo.get("resultCodeId").toString()).split("#");
                res.setApprovalCode(resp[0]);
                res.setMessages(resp[1]);
                if (resultInfo.get("resultStatus").equals("S") && statusDetail.get("acquirementStatus").equals("SUCCESS")) {
                    res.setStatus(Boolean.TRUE);
                    res.setMessages(resp[1]);
                    res.setApprovalCode(bd.get("acquirementId").toString());
                }
            }

        } catch (Exception ex) {
            dataLog.put("response", ex.getMessage());
            ex.printStackTrace();
        }
        new Thread(new Runnable() {

            @Override
            public void run() {
                postResponse(URL_DATA_LOG, gson.toJson(dataLog));
            }
        }).start();
        return res;
    }

    @RequestMapping(value = "/tes", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ApiOperation(value = "Digunakan untuk melakukan Pengetesan Parameter dan Response", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public Response tes(@RequestBody String param) {
        ParameterMapper sh = new ParameterMapper(env);
        SignatureUtility su = new SignatureUtility(env);
        String r = su.agreementCreateSignature("");
        Response res = new Response();
        res.setStatus(Boolean.TRUE);
        res.setMessages("Transaksi Berhasil");
        return res;
    }

    @RequestMapping(value = "/tes-verify", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ApiOperation(value = "Digunakan untuk melakukan Pengetesan Parameter dan Response", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public Response tesVerify(@RequestBody String param) {
//        SignatureUtility su = new SignatureUtility(env);
//        String rs = su.agreementCreateSignature(param);
//        Boolean r = su.AgreementVerifySignature(param);
        Response res = new Response();
        res.setStatus(Boolean.TRUE);
        res.setMessages("Versi 20190216");
        return res;
    }
}
