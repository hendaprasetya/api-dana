/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.dana;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 *
 * @author KFC SOLUTION
 */
@Component
public class SignatureUtility extends SignatureGenerator {

    private final String RSA_PRIVATE_KEY_KFC;
    private final String RSA_PUBLIC_KEY_KFC;
    private final String RSA_PUBLIC_KEY_DANA;

    private static final String BASE_STRING_FORMAT_AGREEMENT_CREATE = "{\"head\":{\"version\":\"1.1.0\",\"clientSecret\":\"Testing clientSecre\","
            + "\"appId\":\"TEST_APP_ID\",\"function\":\"alipay.intl.acquiring.agreement.create\","
            + "\"clientId\":\"211020000000000000011\",\"reqTime\":\"%s\","
            + "\"reqMsgId\":\"%s\"},"
            + "\"body\": {\"productCode\":\"OVERSEA_MULTI_PAYMENT_AGREEMENT\","
            + "\"merchantAgreementId\": \"%s\",\"paymentLoginId\":\"gm_test24@test.com\","
            + "\"bindedMobile\":\"0760000024\",\"merchantUserId\":\"testMerchantUserId\", \"paymentSite\":\"TRUEMONEY\" } }";

    private final String BASE_END_POINT;

    public SignatureUtility(Environment e) {
        this.RSA_PRIVATE_KEY_KFC = e.getRequiredProperty("private.key.kfc");
        this.RSA_PUBLIC_KEY_KFC = e.getRequiredProperty("public.key.kfc");
        this.RSA_PUBLIC_KEY_DANA = e.getRequiredProperty("public.key.dana");
        this.BASE_END_POINT = e.getRequiredProperty("base.end.point");
    }

    public String agreementCreateSignature(String param) {
        String externalSignNo = String.format("%d", (int) (Math.random() * 100000000));
        String createRequest = "";
        try {
            createRequest = generateCreateRequest(param,
                    externalSignNo);
            
//            JSONObject responseObject = JSON.parseObject(createRequest);
//            createRequest = responseObject.getString("signature");
//            Gson gsn = new Gson();
//            Map<String, Object> g = gsn.fromJson(createRequest, new com.google.gson.reflect.TypeToken<Map<String, Object>>() {
//            }.getType());
//            String format = gsn.toJson(g);
            Boolean res = verifySignatureRequest(createRequest);
            System.out.println("OKE : "+res);
            if(res){
                return createRequest;
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(SignatureUtility.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SignatureUtility.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(SignatureUtility.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(SignatureUtility.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(SignatureUtility.class.getName()).log(Level.SEVERE, null, ex);
        }

        return param;
    }

    private String generateCreateRequest(String baseString, String extNo)
            throws NoSuchAlgorithmException,
            UnsupportedEncodingException,
            SignatureException {
        Date currentTime = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        String reqTime = simpleDateFormat.format(currentTime);
        StringBuffer sb = new StringBuffer(reqTime);
        reqTime = sb.insert(sb.length() - 2, ':').toString();
        String reqMsgId = UUID.randomUUID().toString();
        String content = extractSignContent(baseString);
        String contentString = String.format(content, reqTime, reqMsgId, extNo);
        String payloadString = generateRequestString(contentString, RSA_PRIVATE_KEY_KFC);
        return payloadString;
    }

    public Boolean agreementVerifySignature(String response) {
        try {
            String agreementCreateUrl = BASE_END_POINT + "agreementCreate.htm";
//            String createResponse = postRequest(agreementCreateUrl, response);
//            System.out.println("response: " + createResponse);
//            Gson gsn = new Gson();
//            Map<String, Object> g = gsn.fromJson(response, new com.google.gson.reflect.TypeToken<Map<String, Object>>() {
//            }.getType());
//            String format = gsn.toJson(g);
            Boolean status = verifySignature(response);
            return status;
        } catch (IOException ex) {
            Logger.getLogger(SignatureUtility.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(SignatureUtility.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(SignatureUtility.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(SignatureUtility.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(SignatureUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private boolean verifySignature(String responseString) throws InvalidKeyException,
            NoSuchAlgorithmException,
            InvalidKeySpecException,
            SignatureException,
            UnsupportedEncodingException {
        String content = extractSignContent(responseString);
        JSONObject responseObject = JSON.parseObject(responseString);
        String sign = responseObject.getString("signature");

        return verifyRSA2Signature(content, sign, RSA_PUBLIC_KEY_DANA, "UTF-8");
    }
    
    private boolean verifySignatureRequest(String responseString) throws InvalidKeyException,
            NoSuchAlgorithmException,
            InvalidKeySpecException,
            SignatureException,
            UnsupportedEncodingException {
        String content = extractSignContent(responseString);
        JSONObject responseObject = JSON.parseObject(responseString);
        String sign = responseObject.getString("signature");

        return verifyRSA2Signature(content, sign, RSA_PUBLIC_KEY_KFC, "UTF-8");
    }

}
