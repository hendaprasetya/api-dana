/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.dana.model;

import java.util.ArrayList;
import java.util.List;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 *
 * @author KFC SOLUTION
 */
@Component
public class Body {

    String authCode;
    private final String authCodeType;
    private final EnvInfo envInfo;
    private Order order;
    private final String productCode;
    private final String mcc;
    private final ScannerInfo scannerInfo;
    private ShopInfo shopInfo;
    private final String merchantId;
    private final String extendInfo;
    String settleBizDate = "";
//    private final List<NotificationUrl> notificationUrls = new ArrayList<>();

    public Body(Environment e) {
        this.authCodeType = e.getRequiredProperty("bd.auth.type.code");
        this.productCode = e.getRequiredProperty("bd.product.code");
        this.mcc = e.getRequiredProperty("bd.mcc");
        this.merchantId = e.getRequiredProperty("bd.merchant.id");
        this.extendInfo = e.getRequiredProperty("bd.extend.info");
//        String type = e.getRequiredProperty("nu.type");
//        String url = e.getRequiredProperty("nu.url");
//        String[] listType = type.split("#");
//        String[] listUrl = url.split("#");
//        for (int i = 0; i < listType.length; i++) {
//            NotificationUrl nu = new NotificationUrl();
//            nu.type = listType[i];
//            nu.url = listUrl[i];
//            notificationUrls.add(nu);
//        }
        this.envInfo = new EnvInfo(e);
        this.scannerInfo = new ScannerInfo(e);
        this.shopInfo = new ShopInfo(e);
//            this.order = new Order(prm);
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getAuthCodeType() {
        return authCodeType;
    }

    public EnvInfo getEnvInfo() {
        return envInfo;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getMcc() {
        return mcc;
    }

    public ScannerInfo getScannerInfo() {
        return scannerInfo;
    }

    public ShopInfo getShopInfo() {
        return shopInfo;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public String getExtendInfo() {
        return extendInfo;
    }

//    public List<NotificationUrl> getNotificationUrls() {
//        return notificationUrls;
//    }

    public String getSettleBizDate() {
        return settleBizDate;
    }

    public void setSettleBizDate(String settleBizDate) {
        this.settleBizDate = settleBizDate;
    }
}
