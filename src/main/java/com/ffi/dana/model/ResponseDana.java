/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.dana.model;

import com.ffi.param.ParamUtility;

/**
 *
 * @author KFC SOLUTION
 */
public class ResponseDana {

    private static final String AUTH_CODE_ILLEGAL = "AUTH_CODE_ILLEGAL";
    private static final String INVALID_SIGNATURE = "INVALID_SIGNATUR";
    private static final String AUTH_CODE_ALREADY_USED = "AUTH_CODE_ALREADY_USED";
    private static final String USER_STATUS_ABNORMAL = "USER_STATUS_ABNORMAL";
    private static final String RISK_REJECT  = "RISK_REJECT";
    private static final String ORDER_IS_CLOSED  = "ORDER_IS_CLOSED";
    private static final String ORDER_STATUS_INVALID = "ORDER_STATUS_INVALID";
    private static final String MERCHANT_AND_SHOP_NOT_MATCH  = "MERCHANT_AND_SHOP_NOT_MATCH";
    private static final String SHOP_NOT_EXIST  = "SHOP_NOT_EXIST";
    private static final String SHOP_STATUS_ABNORMAL  = "SHOP_STATUS_ABNORMAL";
    private static final String USER_PAYING  = "USER_PAYING";
    private static final String PAYMENT_IN_PROCESS   = "PAYMENT_IN_PROCESS";
    private static final String SYSTEM_ERROR   = "SYSTEM_ERROR";
    private static final String TARGET_NOT_FOUND = "TARGET_NOT_FOUND";
    private static final String INIT = "INIT";
    private static final String REPEAT_REQ_INCONSISTENT = "REPEAT_REQ_INCONSISTENT";
    private static final String BALANCE_NOT_ENOUGH = "BALANCE_NOT_ENOUGH";
    private static final String OTHER_FAIL_RESULT_CODE = "OTHER_FAIL_RESULT_CODE";
    private static final String TIME_OUT = "TIME_OUT";
    
    private static final String MSG_REPEAT_REQ_INCONSISTENT = "Transaksi belum berhasil. Tekan Tombol Online Check atau batalkan ?";
    private static final String MSG_AUTH_CODE_ILLEGAL = "Transaksi gagal. Silahkan Refresh dan Ulangi Scan QR";
    private static final String MSG_AUTH_CODE_ALREADY_USED = "Transaksi belum berhasil. Tekan Tombol Online Check atau batalkan ?";
    private static final String MSG_BALANCE_NOT_ENOUGH = "Saldo DANA tidak cukup. Coba metode pembayaran lain.";
    private static final String MSG_RISK_REJECT = "Transaksi gagal. Coba metode pembayaran lain.";
    private static final String MSG_USER_PAYING =  "Transaksi sedang diproses. Tekan Tombol Online Check atau batalkan ?";
    private static final String MSG_PAYMENT_IN_PROCESS = "Transaksi sedang diproses. Tekan Tombol Online Check atau batalkan ?";
    private static final String MSG_SYSTEM_ERROR = "Terdapat gangguan. Tekan Tombol Online Check atau batalkan ? ";
    private static final String MSG_OTHER_FAIL_RESULT_CODE = "Transaksi gagal. Coba metode pembayaran lain.";
    private static final String MSG_TIME_OUT = "Terdapat gangguan. Tekan Tombol Online Check atau batalkan ?";
    private static final String MSG_DEFAULT = "Transaksi gagal. Silahkan Refresh dan Ulangi Scan QR";
    
    private final String response;
    private final String message;

    public ResponseDana(String response) {
        if(response.equals(AUTH_CODE_ILLEGAL)){
            this.response = ParamUtility.RES_RESCAN_MEMBER;
            this.message = MSG_AUTH_CODE_ILLEGAL;
        }else if(response.equals(INVALID_SIGNATURE)){
            this.response = ParamUtility.RES_RESCAN_MEMBER;
            this.message = MSG_AUTH_CODE_ILLEGAL;
        }else if(response.equals(AUTH_CODE_ALREADY_USED)){
            this.response = ParamUtility.RES_BILL_READY_QUERY;
            this.message = MSG_AUTH_CODE_ALREADY_USED;
        }else if(response.equals(USER_STATUS_ABNORMAL)){
            this.response = ParamUtility.RES_PAYMENT_ERROR;
            this.message = MSG_DEFAULT;
        }else if(response.equals(RISK_REJECT)){
            this.response = ParamUtility.RES_PAYMENT_ERROR;
            this.message = MSG_RISK_REJECT;
        }else if(response.equals(ORDER_IS_CLOSED)){
            this.response = ParamUtility.RES_PAYMENT_ERROR;
            this.message = MSG_DEFAULT;
        }else if(response.equals(ORDER_STATUS_INVALID)){
            this.response = ParamUtility.RES_PAYMENT_ERROR;
            this.message = MSG_DEFAULT;
        }else if(response.equals(MERCHANT_AND_SHOP_NOT_MATCH)){
            this.response = ParamUtility.RES_PAYMENT_ERROR;
            this.message = MSG_DEFAULT;
        }else if(response.equals(SHOP_NOT_EXIST)){
            this.response = ParamUtility.RES_PAYMENT_ERROR;
            this.message = MSG_DEFAULT;
        }else if(response.equals(SHOP_STATUS_ABNORMAL)){
            this.response = ParamUtility.RES_PAYMENT_ERROR;
            this.message = MSG_DEFAULT;
        }else if(response.equals(USER_PAYING)){
            this.response = ParamUtility.RES_BILL_READY_QUERY;
            this.message = MSG_USER_PAYING;
        }else if(response.equals(PAYMENT_IN_PROCESS)){
            this.response = ParamUtility.RES_BILL_READY_QUERY;
            this.message = MSG_PAYMENT_IN_PROCESS;
        }else if(response.equals(SYSTEM_ERROR)){
            this.response = ParamUtility.RES_BILL_READY_QUERY;
            this.message = MSG_SYSTEM_ERROR;
        }else if(response.equals(TARGET_NOT_FOUND)){
            this.response = ParamUtility.RES_RESCAN_MEMBER;
            this.message = MSG_AUTH_CODE_ILLEGAL;
        }else if(response.equals(INIT)){
            this.response = ParamUtility.RES_BILL_READY_QUERY;
            this.message = MSG_USER_PAYING;
        }else if(response.equals(REPEAT_REQ_INCONSISTENT)){
            this.response = ParamUtility.RES_BILL_READY_QUERY;
            this.message = MSG_REPEAT_REQ_INCONSISTENT;
        }else if(response.equals(BALANCE_NOT_ENOUGH)){
            this.response = ParamUtility.RES_PAYMENT_ERROR;
            this.message = MSG_BALANCE_NOT_ENOUGH;
        }else if(response.equals(OTHER_FAIL_RESULT_CODE)){
            this.response = ParamUtility.RES_PAYMENT_ERROR;
            this.message = MSG_OTHER_FAIL_RESULT_CODE;
        }else if(response.equals(TIME_OUT)){
            this.response = ParamUtility.RES_BILL_READY_QUERY;
            this.message = MSG_TIME_OUT;
        }else{
            this.response = ParamUtility.RES_PAYMENT_ERROR;
            this.message = MSG_DEFAULT;
        }        
    }

    public String getResponse() {
        return response;
    }

    public String getMessage() {
        return message;
    }
}
