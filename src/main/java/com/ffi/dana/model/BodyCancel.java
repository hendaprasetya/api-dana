/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.dana.model;

import org.springframework.core.env.Environment;

/**
 *
 * @author KFC SOLUTION
 */
public class BodyCancel {

    private final String merchantId;
    String acquirementId;
    String cancelReason;
    String merchantTransId;

    public BodyCancel(Environment e) {
        this.merchantId = e.getRequiredProperty("bd.merchant.id");
    }

    public String getMerchantId() {
        return merchantId;
    }

    public String getAcquirementId() {
        return acquirementId;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setAcquirementId(String acquirementId) {
        this.acquirementId = acquirementId;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getMerchantTransId() {
        return merchantTransId;
    }

    public void setMerchantTransId(String merchantTransId) {
        this.merchantTransId = merchantTransId;
    }
}
