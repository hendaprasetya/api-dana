/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.dana.model;

/**
 *
 * @author KFC SOLUTION
 */
public class ParamOrderItem {
    
    String merchantGoodsId = "";
    String description = "";
    String category = "";
    String unit = "PCS";
    String quantity = "0";
    Price price = new Price();
    

    public String getMerchantGoodsId() {
        return merchantGoodsId;
    }

    public void setMerchantGoodsId(String merchantGoodsId) {
        this.merchantGoodsId = merchantGoodsId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }
    
    public class Price{
        String value = "0";
        String currency = "IDR";

        public String getValue() {
            return value;
        }

        public void setValue(Double value) {
            long Total = Math.round(value+(value*0.1));
            int round_limit = 100;
            int round_factor = 50;
            long BillRounding = 0;
            if (Total % round_limit >= round_factor) {
                BillRounding = round_limit - (Total % round_limit);

            } else {
                BillRounding = -(Total % round_limit);
            }
            Total = (Total + BillRounding)*100;
//            Integer sales = (Integer) Total;
//            value = Math.round(value+(value*0.1))*100d;
            this.value = String.valueOf(Total).replace(".0", "");
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }
    }
}
