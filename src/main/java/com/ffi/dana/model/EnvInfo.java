/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.dana.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 *
 * @author KFC SOLUTION
 */
@Component
public class EnvInfo {

    private final String terminalType;
//    private final String osType;
//    private final String extendInfo;
//    private final String orderOsType;
//    private final String sdkVersion;
//    private final String websiteLanguage;
//    private final String tokenId;
//    private final String sessionId;
//    private final String appVersion;
//    private final String merchantAppVersion;
//    private final String clientKey;
    private final String orderTerminalType;
//    private final String clientIp;
    
    @Autowired
    public EnvInfo(Environment e){
        this.terminalType = e.getRequiredProperty("ei.terminal.type");
//        this.osType = e.getRequiredProperty("ei.os.type");
//        this.extendInfo = e.getRequiredProperty("ei.extend.info");
//        this.orderOsType = e.getRequiredProperty("ei.order.os.type");
//        this.sdkVersion = e.getRequiredProperty("ei.sdk.version");
//        this.websiteLanguage = e.getRequiredProperty("ei.website.language");
//        this.tokenId = e.getRequiredProperty("ei.token.id");
//        this.sessionId = e.getRequiredProperty("ei.session.id");
//        this.appVersion = e.getRequiredProperty("ei.app.version");
//        this.merchantAppVersion = e.getRequiredProperty("ei.merchant.app.version");
//        this.clientKey = e.getRequiredProperty("ei.client.key");
        this.orderTerminalType = e.getRequiredProperty("ei.order.terminal.type");
//        this.clientIp = e.getRequiredProperty("ei.client.ip");
        
    }

    public String getTerminalType() {
        return terminalType;
    }
//
//    public String getOsType() {
//        return osType;
//    }
//
//    public String getExtendInfo() {
//        return extendInfo;
//    }
//
//    public String getOrderOsType() {
//        return orderOsType;
//    }
//
//    public String getSdkVersion() {
//        return sdkVersion;
//    }
//
//    public String getWebsiteLanguage() {
//        return websiteLanguage;
//    }
//
//    public String getTokenId() {
//        return tokenId;
//    }
//
//    public String getSessionId() {
//        return sessionId;
//    }
//
//    public String getAppVersion() {
//        return appVersion;
//    }
//
//    public String getMerchantAppVersion() {
//        return merchantAppVersion;
//    }
//
//    public String getClientKey() {
//        return clientKey;
//    }

    public String getOrderTerminalType() {
        return orderTerminalType;
    }

//    public String getClientIp() {
//        return clientIp;
//    }
}
