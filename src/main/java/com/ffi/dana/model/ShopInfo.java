/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.dana.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 *
 * @author KFC SOLUTION
 */
@Component
public class ShopInfo {

    private final String operatorId;
    private String externalShopId;
    
    @Autowired
    public ShopInfo(Environment env){
        this.operatorId = env.getRequiredProperty("si.operator.id");
//        this.shopId = env.getRequiredProperty("si.shop.id");
    }

    public String getOperatorId() {
        return operatorId;
    }

    public String getExternalShopId() {
        return externalShopId;
    }

    public void setExternalShopId(String externalShopId) {
        this.externalShopId = externalShopId;
    }

}
