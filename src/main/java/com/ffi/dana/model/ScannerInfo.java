/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.dana.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 *
 * @author KFC SOLUTION
 */
@Component
public class ScannerInfo {

    private final String deviceIp;
    private final String deviceModel;
    private final String deviceVersion;
    String deviceId;
    
    @Autowired
    public ScannerInfo(Environment e){
        this.deviceIp = e.getRequiredProperty("si.device.ip");
        this.deviceModel = e.getRequiredProperty("si.device.model");
        this.deviceVersion = e.getRequiredProperty("si.device.version");
    }

    public String getDeviceIp() {
        return deviceIp;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public String getDeviceVersion() {
        return deviceVersion;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
