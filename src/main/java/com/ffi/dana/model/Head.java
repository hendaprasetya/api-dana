/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.dana.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 *
 * @author KFC SOLUTION
 */
@Component
public class Head {

//    private final String accessToken;
    private final String reqTime;
    private final String reqMsgId;
//    private final String reserve;
    private final String clientSecret;
    private final String clientId;
    private String function;
    private final String version;
    
    public Head(Environment e){
//        this.accessToken = e.getRequiredProperty("hd.access.token");
        this.reqMsgId = UUID.randomUUID().toString();
//        this.reserve = e.getRequiredProperty("hd.reserve");
        this.clientSecret = e.getRequiredProperty("hd.client.secret");
        this.clientId = e.getRequiredProperty("hd.client.id");
        this.function = e.getRequiredProperty("hd.function");
//        if(act.equals("payment")){
//            this.function = e.getRequiredProperty("hd.function").concat(".retail.pay");
//        }else if(act.equals("cancel")){
//            this.function = e.getRequiredProperty("hd.function").concat(".order.cancel");
//        }else{
//            this.function = e.getRequiredProperty("hd.function").concat(".retail.pay");
//        }
        this.version = e.getRequiredProperty("hd.version");
        this.reqTime = dateFormat(new Date());
    }

//    public String getAccessToken() {
//        return accessToken;
//    }
    
    private String dateFormat(Date date){
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        
        Date dt = calendar.getTime();
        
        String formatted = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .format(dt);
        return formatted.substring(0, 22) + ":" + formatted.substring(22);
    }

    public String getReqTime() {
        return reqTime;
    }

    public String getReqMsgId() {
        return reqMsgId;
    }

//    public String getReserve() {
//        return reserve;
//    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getClientId() {
        return clientId;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getVersion() {
        return version;
    }
}
