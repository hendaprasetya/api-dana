/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.dana.model;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 *
 * @author KFC SOLUTION
 */
@Component
public class ParameterCancel {
    
    Head head;
    BodyCancel body;
//    private final String signature;
    
    public ParameterCancel(Environment e){
        this.head = new Head(e);
        head.setFunction(head.getFunction().concat(".order.cancel"));
//        this.signature = e.getRequiredProperty("signature");
    }
    
    

    public Head getHead() {
        return head;
    }

//    public String getSignature() {
//        return signature;
//    }

    public BodyCancel getBody() {
        return body;
    }

    public void setBody(BodyCancel body) {
        this.body = body;
    }
}
