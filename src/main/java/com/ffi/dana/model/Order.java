/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.dana.model;

import com.ffi.param.ParamPayment;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * @author KFC SOLUTION
 */
public class Order {

    Date expiryTime;
    String merchantTransType;
    String orderTitle;
    String merchantTransId;
//    String orderMemo;
    String createdTime;
    OrderAmount orderAmount;
//    Buyer buyer;
    List<ParamOrderItem> goods = new ArrayList<>();
    
    public Order(ParamPayment param){
        this.merchantTransType = "KFC_QR";//param.getOutlet().getOrderType();
        this.orderTitle = "KFC QR Payment";
        this.merchantTransId = param.getBillNo();
//        this.orderMemo="";
        this.createdTime = dateFormat(new Date());
        OrderAmount oa = new OrderAmount();
        oa.setCurrency("IDR");
        oa.setValue(param.getTotalAmount().concat("00"));
//        oa.setValue("80000");
        this.orderAmount = oa;
//        Buyer by = new Buyer();
//        by.setUserId(param.getOutlet().getOutletCode());
//        by.setExternalUserId(param.getOutlet().getRegionCode());
//        by.setNickname(param.getOutlet().getPosCode());
//        by.setExternalUserType(param.getOutlet().getOrderType());
//        this.buyer = by;
        List<com.ffi.param.ParamOrderItem> listItem = param.getListItem();
        for(com.ffi.param.ParamOrderItem i : listItem){
            ParamOrderItem po = new ParamOrderItem();
            po.setCategory("");
            po.setDescription(i.getItem_name());
            po.setMerchantGoodsId("");
            po.getPrice().setValue(i.getItem_unit_price());
            po.setQuantity(i.getItem_quantity().toString());
            goods.add(po);
        }
    }
    
    private String dateFormat(Date date){
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS:Z");
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        
        Date dt = calendar.getTime();
        
        String formatted = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .format(dt);
        System.out.println("XXX ==>"+formatted.substring(0, 22) + ":" + formatted.substring(22));
        return formatted.substring(0, 22) + ":" + formatted.substring(22);
    }

    public Date getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(Date expiryTime) {
        this.expiryTime = expiryTime;
    }

    public String getMerchantTransType() {
        return merchantTransType;
    }

    public void setMerchantTransType(String merchantTransType) {
        this.merchantTransType = merchantTransType;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public String getMerchantTransId() {
        return merchantTransId;
    }

    public void setMerchantTransId(String merchantTransId) {
        this.merchantTransId = merchantTransId;
    }

//    public String getOrderMemo() {
//        return orderMemo;
//    }
//
//    public void setOrderMemo(String orderMemo) {
//        this.orderMemo = orderMemo;
//    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public OrderAmount getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(OrderAmount orderAmount) {
        this.orderAmount = orderAmount;
    }

//    public Buyer getBuyer() {
//        return buyer;
//    }
//
//    public void setBuyer(Buyer buyer) {
//        this.buyer = buyer;
//    }
    
    
    
    private class OrderAmount{
        String value;
        String currency;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }
    }
    private class Buyer{
        String externalUserType;
        String nickname;
        String externalUserId;
        String userId;

        public String getExternalUserType() {
            return externalUserType;
        }

        public void setExternalUserType(String externalUserType) {
            this.externalUserType = externalUserType;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getExternalUserId() {
            return externalUserId;
        }

        public void setExternalUserId(String externalUserId) {
            this.externalUserId = externalUserId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }
    }

    public List<ParamOrderItem> getGoods() {
        return goods;
    }

    public void setGoods(List<ParamOrderItem> goods) {
        this.goods = goods;
    }
}
