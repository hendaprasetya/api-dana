/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.dana.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 *
 * @author KFC SOLUTION
 */
@Component
public class ParameterMapper {

    Body body;
    Head head;
//    private final String signature;
    
    @Autowired
    public ParameterMapper(Environment e){
//        body = new Body(e);
        head = new Head(e);
        head.setFunction(head.getFunction().concat(".retail.pay"));
//        signature = e.getRequiredProperty("signature");
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

//    public String getSignature() {
//        return signature;
//    }
    
}
