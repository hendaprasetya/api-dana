/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.dana;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author KFC SOLUTION
 */
public class SignatureGenerator {
    
    private static final String UTF_8               = "UTF-8";
    private static final String SIGN_ALGORITHMS     = "SHA256withRSA";
    private static final String RSA                 = "RSA";
    private static final String BASE_REQUEST_STRING = "{\"request\": %s, \"signature\":\"%s\"}";

    public String extractSignContent(String message) {
        // NOTE: at this point, we could safely assume that the JSON request message have
        // the correct A+ request structure. Hence, we could simply extract the content between the second-level
        // '{ ... }' to get the content that was used to generate the signature.

        int idx = message.indexOf('{');
        int beginIndex = message.indexOf('{', idx + 1);

        idx = message.lastIndexOf('}');
        int endIndex = message.lastIndexOf('}', idx - 1);

        return message.substring(beginIndex, endIndex + 1);
    }

    public boolean verifyRSA2Signature(String content, String sign, String publicKey, String charset)
                                                                                                     throws NoSuchAlgorithmException,
                                                                                                     InvalidKeySpecException,
                                                                                                     InvalidKeyException,
                                                                                                     SignatureException,
                                                                                                     UnsupportedEncodingException {
        byte[] decodedKey = Base64.decodeBase64(publicKey.getBytes());
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(decodedKey);
        KeyFactory keyFactory = KeyFactory.getInstance(RSA);
        PublicKey pubKey = keyFactory.generatePublic(keySpec);
        Signature signature = Signature.getInstance(SIGN_ALGORITHMS);
        signature.initVerify(pubKey);
        signature.update(getContentBytes(content, charset));
        return signature.verify(Base64.decodeBase64(sign.getBytes()));
    }

    /**
     * sign
     *
     * @param content
     * @param privateKey
     * @return String
     * @throws SignatureException
     */
    public static String SHA256withRSA(String content, String privateKey) throws SignatureException {

        try {
            KeyFactory keyFactory = KeyFactory.getInstance(RSA);
            // base64
            byte[] decodedKey = Base64.decodeBase64(privateKey.getBytes());
            // PKCS8Encoded
            PrivateKey priKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(decodedKey));
            Signature signature = Signature.getInstance(SIGN_ALGORITHMS);
            signature.initSign(priKey);
            signature.update(getContentBytes(content, UTF_8));

            byte[] signed = signature.sign();

            return new String(Base64.encodeBase64(signed));
        } catch (Exception e) {
            throw new SignatureException("RSA content = " + content + "; charset = " + UTF_8
                                         + " exception!", e);
        }
    }

    /**
     *
     *
     * @param content
     * @param charset
     * @return
     * @throws UnsupportedEncodingException
     */
    private static byte[] getContentBytes(String content, String charset)
                                                                         throws UnsupportedEncodingException {
        if (charset == null || charset.isEmpty()) {
            return content.getBytes();
        }

        return content.getBytes(charset);
    }

    /**
     * Generate the isupergw request string
     *
     * @param content
     * @param privateKey
     * @return
     * @throws SignatureException
     */
    public static String generateRequestString(String content, String privateKey)
                                                                                 throws SignatureException {
        String signature = SHA256withRSA(content, privateKey);
        String requestString = String.format(BASE_REQUEST_STRING, content, signature);
        return requestString;
    }

    protected String postRequest(String urlString, String payloadString) throws IOException {
        URL url = new URL(urlString);
        String type = "application/json";
        //Make connection
        HttpURLConnection urlc = (HttpURLConnection) url.openConnection();

        //use get mode
        urlc.setDoOutput(true);
        urlc.setRequestMethod("POST");
        urlc.setAllowUserInteraction(false);
        urlc.setRequestProperty("Content-Type", type);
        urlc.setRequestProperty("Charset", "UTF-8");

        //get result
        DataOutputStream outputStream = new DataOutputStream(urlc.getOutputStream());
        outputStream.writeBytes(payloadString);
        outputStream.flush();
        outputStream.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        System.out.println(response);

        return response.toString();

    }
}
